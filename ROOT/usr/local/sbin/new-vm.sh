#!/bin/bash

# to create virtual machines on LVM or DRBD
SSH="ssh twin"
SCP='scp'
XM='xl'
ME=`hostname`

# An early sanity check!
# This is deliberately before we set -e for the main flow of the script.
mount | grep -q /mnt/xen

if [ $? -eq 0 ] ; then
 echo /mnt/xen is currently mounted, refusing to proceed.
 exit 1
fi

set -e 

### Functions

# Script:
function usage() {
 cat <<EOF
 Usage: $0 -h Hostname (FQDN) [-o] [-r] [-s size/GB] [-v volume_group] [-m memory/MB] [-d DIST] [-w] [-p DRBD_PRIORITY] [-c CPUS] [-l LV_NAME] [-x]
 builds a virtual machine. 
 -h fully-qualified hostname (NB local part should be <=15 chars if you plan to winbind)
 -o over-writes any existing config files if the machine is not already running
 -r reinstall existing VM rather than setting up a new one
 -s disk size in GB (default: 10)
 -v volume group name
 -m RAM for the VM (default: 768)
 -d focal/jammy/noble/bookworm/none . default=noble (none is for Windows)
 -w Configure as a workstation (rather than server)
 -p set sync-after priority for DRBD (default: 10)
 -c number of cpus
 -l set the name of the logical volume, also used for the drbd (default: short host name)
 -x debug mode

 When reinstalling, you must pass the same LV_NAME as was used initially (unless that was the default).
EOF
 exit 1
}


### Functions which ensure the world is ready for this machine

 ##
 # Check whether a named DRBD device exists
 ##
function drbdexists() {
 local LVNAME=$1
 ((drbdadm state $LVNAME >/dev/null 2>&1) &&
  (! drbdadm state $LVNAME 2>&1 | grep -q "no resources defined" ))
}

 ##
 # Set MAC address from database, bale if not found 
 ##
function ensuremac() {
 local NEWHOST=$1
 MACADDR=$(wget -q -O - http://os-installer-utils.ch.cam.ac.uk/osinst/api/getmac?hostname=$NEWHOST | jq -r ".mac")
 if [ -z $MACADDR ] ; then
  echo No MAC address could be found.
  usage
  exit 1
 fi
}

## ensure that our MAC address is not already in use
function checkuniquemac() {
 local MACADDR=$1
 JSON=$(wget -q -O - "http://os-installer-utils.ch.cam.ac.uk/osinst/api/checkuniquemac?mac=$MACADDR")
 ISUNIQUE=$(echo $JSON | jq '.isunique')
 if [ "$ISUNIQUE" == 0 ] ; then
  echo Error: $MACADDR is not unique
 fi
}

 ##
 # Check we can resolve this hostname (i.e. there's an IP address set)
 # and bale if not.
 ##
function ensuredns() {
 NEWHOST=$1
 # Is hostname in DNS yet?
 if ! getent hosts $NEWHOST >/dev/null 2>&1 ; then
 cat <<EOF
 * Cannot resolve $NEWHOST. Not proceeding further. 
 * (you could always add to /etc/hosts if required)
EOF
 exit 3
fi
}

 ## 
 # Check we haven't already created a DRBD device with this name
 # Bale out if we have
 ## 
function ensurenodrbd() {
 local LVNAME=$1
 if drbdexists $LVNAME ; then
  cat <<EOF
  * The lv name ($LVNAME) is already in use, according to DRBD.
  * Stop the drbd, edit /etc/drbd.conf and remove references to it if
  * you're sure that it's not in use.
umount -f `drbdadm sh-dev $LVNAME`
drbdadm down $LVNAME && rm /etc/drbd.d/$LVNAME.res
$SSH drbdadm down $LVNAME && $SSH rm /etc/drbd.d/$LVNAME.res
EOF
  exit 5
 fi
 if (grep ^resource /etc/drbd.conf | grep -q '[ "]'$LVNAME'[ "]' ) ; then
  cat <<EOF
  * $LVNAME is still listed in /etc/drbd.conf. You need to remove that before continuing.
EOF
  exit 5
 fi
 if ([ -d /etc/drbd.d/ ] && [ -f /etc/drbd.d/${LVNAME}.res ]) ||
    ($SSH [ -d /etc/drbd.d/ ] && $SSH [ -f /etc/drbd.d/${LVNAME}.res ]) ; then
  if ($OVERWRITE) ; then
  cat <<EOF
   * Removing configuration at /etc/drbd.d/${LVNAME}.res.
EOF
   rm -f /etc/drbd.d/${LVNAME}.res
   $SSH rm -f /etc/drbd.d/${LVNAME}.res
  fi;
  if [ -d /etc/drbd.d/ ] && [ -f /etc/drbd.d/${LVNAME}.res ] ; then
   cat <<EOF
   * Failed to remove configuration at /etc/drbd.d/${LVNAME}.res.
EOF
   exit 5
  fi
 fi
 
}

 ##
 # Check we haven't already created a DomU with this name, bale if we have
 ##
function ensurenovm() {
 local NEWHOST=$1
 # Does that VM exist?
 if ping  -q -c 1 -W 1 $NEWHOST >/dev/null 2>&1 ; then
cat <<EOF
 $NEWHOST is pingable. Find it and shut it down if you want to proceed.
 If $NEWHOST is running here, you might wish to
 * xl shutdown -w $NEWHOST
 * new-vm.sh -h $NEWHOST -r [-d DIST] [-m MEMORY] [-w]
 or, run new-vm.sh with no flags to check the usage options.
EOF
  exit 4
 fi
}

 ##
 # Over-long names are bad for Windows domains. Check this.
 ##
function warnifnametoolong() {
 local SHORTHOST=$1
 if [ ${#SHORTHOST} -gt 15 ] ; then
  cat <<EOF
  * ${SHORTHOST} is ${#SHORTHOST} characters long. Names longer than 15 characters
  * cannot be joined to the Active Directory, so you may want to choose something
  * shorter if you plan to winbind this VM.
EOF
 fi
}

function getvlan() {
  VLAN=$(wget -O - http://linux-builder.ch.cam.ac.uk/ansible/vlan.php?mac=$MACADDR |head -1) 
  if [ -z $VLAN ]
  then
    echo No VLAN found, using vlan 1
    VLAN=1
  fi
}

### Functions which adjust the Dom0 to handle this machine

 ##
 # To check that we have a few tools which might not be here
 ##
function checkbinaries() {
 debootstrap --version
 pvdisplay --version
}

 ##
 # Boolean function to check whether DRBD is in use
 ##
function checkdrbd() {
 if [ -f /proc/drbd ] ; then
  DRBDINUSE=true
 else
  DRBDINUSE=false
 fi
}

 ## 
 # Configure heartbeat
 ##
function configha() {
 # Add this machine to heartbeat
 sed 's/'$ME'.*/& xenlive::'$NEWHOST'/' -i /etc/ha.d/haresources
 $SCP /etc/ha.d/haresources root@twin:/etc/ha.d/
}

function syncxenconf() {
 # Copy the config to twin
 $SCP /etc/xen/$NEWHOST root@twin:/etc/xen/$NEWHOST
}
 
 ##
 # Creates a DRBD device, set BLOCKDEV
 ##
function createdrbd() {
 local LVNAME=$1
 if [ -d /etc/drbd.d/ ] ; then
  newdrbd $LVNAME $VGROUP >>/etc/drbd.d/${LVNAME}.res
  /usr/local/sbin/make-drbds-sync-after.sh
  drbdadm adjust all || true
  $SCP /etc/drbd.d/*.res root@twin:/etc/drbd.d/
  $SSH drbdadm adjust all || true
 else 
  cp /etc/drbd.conf /etc/drbd.conf-pre-$LVNAME
  $SSH cp /etc/drbd.conf /etc/drbd.conf-pre-$LVNAME
  newdrbd $LVNAME $VGROUP >>/etc/drbd.conf
  $SCP /etc/drbd.conf root@twin:/etc/drbd.conf
 fi
 # Start the drbd
 yes | drbdadm create-md $LVNAME
 yes | $SSH drbdadm create-md $LVNAME
 drbdadm up $LVNAME
 $SSH drbdadm up $LVNAME
 DEVICE=`drbdadm dump $LVNAME | grep -A2 $ME | grep device | awk ' { print $2 ; } ' | sed s/\;//`
 drbdsetup $DEVICE primary --overwrite-data-of-peer
 sleep 6
 drbdadm secondary $LVNAME
 BLOCKDEV=`drbdadm sh-dev $LVNAME`
}

function getblockdev() {
 if $DRBDINUSE ; then
  drbdadm primary $LVNAME
  BLOCKDEV=`drbdadm sh-dev $LVNAME`
 else
  BLOCKDEV=/dev/${VGROUP}/${LVNAME}
 fi
}

 ##
 # Create logical volume on this node, and on twin IFF drbd is in use 
 ##
function createlv() {
 local LVNAME=$1
 local VGROUP=$2
 local DISKSIZE=$3
 ensurenolv $LVNAME
 # Create the logical volume
 lvcreate --size $DISKSIZE --name $LVNAME $VGROUP
 if $DRBDINUSE; then
  $SSH lvcreate --size $DISKSIZE --name $LVNAME $VGROUP
 fi
 # And blank them
 MDSIZE=500000
 SIZE=`lvs --units b /dev/${VGROUP}/${LVNAME} | grep $LVNAME | awk ' { print \$4 ; } ' | sed s/B//`
 SKIP=$((SIZE-MDSIZE))
 dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE skip=$SKIP
 dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE seek=$SKIP
 if $DRBDINUSE ; then
  $SSH dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE skip=$SKIP
  $SSH dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE seek=$SKIP
 fi
}

 ##
 # We haven't already got an LV of that name, have we?
 ##
function ensurenolv() {
 local LVNAME=$1
 # Does that VG exist?
 # (That grep call is needed - vgdisplay of a missing vg returns 0)
 if ! vgdisplay $VGROUP 2>&1 | grep -q Metadata ; then
  cat <<EOF
  * That volume group does not exist. Please choose from:
EOF
  vgdisplay | grep Name | awk ' { print $3 ; } '
  exit 6
 fi
 # Does that LV exist?
 if [ -b /dev/${VGROUP}/${LVNAME} ]; then
  if ($OVERWRITE) ; then
   if (mount | grep -q /dev/${VGROUP}/${LVNAME}) then
    if lsof /dev/${VGROUP}/${LVNAME} >/dev/null 2>&1 ; then
     lsof /dev/${VGROUP}/${LVNAME}  | awk ' { print $2 ; } ' | grep -v PID| xargs -r kill -9 
    fi
    umount -f /dev/${VGROUP}/${LVNAME} || true
   fi
   lvremove -f /dev/${VGROUP}/${LVNAME}
   if $DRBDINUSE ; then
    $SSH lvremove -f /dev/${VGROUP}/${LVNAME}
   fi
  else
   cat <<EOF
  * That logical volume already exists. Perhaps you need to remove it with 
  * lvremove /dev/${VGROUP}/${LVNAME}
EOF
   exit 7
  fi
 fi
}

 ##
 # Creates a new DRBD device
 ##
function newdrbd() {
 local LVNAME=$1
 # //FOO/BAR replaces all FOO with BAR, not just 1st FOO
 local VGROUP=$2
 THISHOST=`hostname`
 TWINHOST=`$SSH hostname`
 LASTDEV=`cat /proc/drbd | grep '[ds:|ro:]' | awk ' { print $1 ; } ' | sed s/:// | sort -n | tail -1`
 THISDEV=drbd$((LASTDEV+1))
 LASTDEV=`$SSH cat /proc/drbd | grep '[ds:|ro:]' | awk ' { print $1 ; } ' | sed s/:// | sort -n | tail -1`
 TWINDEV=drbd$((LASTDEV+1))
 TWINIP=$(getent hosts twin | awk '{print $1}')
 REPNET_INTERFACE=$(ip route get $TWINIP | grep $TWINIP | awk '{print $3}')
 THISIP=`ip address show dev $REPNET_INTERFACE | grep 'inet ' | awk ' { print $2 ; } '| cut -d/ -f1`
 # if this is the first drbd need to start port numbers at 7789 or something
 LASTPORT=`drbdadm dump | grep $THISIP | sed s/.*:// | sed s/\;// | sort -n | tail -1`
 if [ -z "$LASTPORT" ] ; then LASTPORT=7788; fi
 THISPORT=$((LASTPORT+1))
 LASTPORT=`$SSH drbdadm dump | grep $TWINIP | sed s/.*:// | sed s/\;// | sort -n | tail -1`
 if [ -z "$LASTPORT" ] ; then LASTPORT=7788; fi
 TWINPORT=$((LASTPORT+1))
 
cat <<EOF
resource "$LVNAME" {
EOF

if [ -n "$LASTDEV" ] ; then
cat <<EOF
 syncer {
 # Priority $DRBD_PRIORITY
 after foo;
 }
EOF
fi
cat <<EOF
 on $THISHOST {
  device        /dev/$THISDEV;
  disk          /dev/${VGROUP}/${LVNAME};
  address       $THISIP:$THISPORT;
  meta-disk     internal;
 }
 on $TWINHOST {
  device        /dev/$TWINDEV;
  disk          /dev/${VGROUP}/${LVNAME};
  address       $TWINIP:$TWINPORT;
  meta-disk     internal;
 }
}

EOF
}

 ##
 # Writes config for Xen
 ##
function writexenconfig() {
 local DISTRO=$1
 if $DRBDINUSE ; then
  DISK="['drbd:$LVNAME,xvda1,w']"
 else
  if [ "$XM" == "xl" ] ; then
   DISK="['$BLOCKDEV,raw,xvda1,w']"
  else
   DISK="['phy:$BLOCKDEV,xvda1,w']"
  fi
 fi

if [[ "$DIST" == "none" ]]; then
  cat >/etc/xen/$NEWHOST <<EOF
builder='hvm'
boot='c'

vnc=1
usbdevice='tablet'
EOF
else
  cat >/etc/xen/$NEWHOST <<EOF
bootloader	= 'pygrub'
EOF
fi

cat >>/etc/xen/$NEWHOST <<EOF
memory = '$MEMORY'

# Disks
disk = $DISK

# Hostname
name='$NEWHOST'

# Networking - ensure that the MAC address is unique.
vif=['bridge=vlanbr$VLAN, mac=$MACADDR']

on_poweroff = 'destroy'
on_reboot   = 'restart'
on_crash    = 'restart'

vncpasswd = ''

cpus = 'all,^0-1'

vcpus = $CPUS
EOF
}

function unmountchroot() {
 local DIR=$1
 sleep 2
 umount $DIR ||true
}

### Functions which operate directly in the DomU

function gettyonconsole() {
 local DIR=$1
 echo '7:2345:respawn:/sbin/getty 38400 hvc0' >> $DIR/etc/inittab
}

function sourceslist() {
 local DIR=$1
 local DISTRO=$2
 local FAMILY=$3
 if [ "$FAMILY" == "debian" ];then
   SECTIONS="main contrib non-free non-free-firmware"
   BASE="http://mirror.apps.cam.ac.uk/pub/linux/${FAMILY} $DISTRO $SECTIONS"
   UPDATES="http://mirror.apps.cam.ac.uk/pub/linux/${FAMILY} ${DISTRO}-updates $SECTIONS"
   SECURITY="http://security.debian.org/ ${DISTRO}-security ${SECTIONS}"
 else
   # assume ubuntu
   SECTIONS="main restricted universe multiverse"
   BASE="http://mirror.apps.cam.ac.uk/pub/linux/${FAMILY} $DISTRO $SECTIONS"
   UPDATES="http://mirror.apps.cam.ac.uk/pub/linux/${FAMILY} ${DISTRO}-updates $SECTIONS"
   SECURITY="http://mirror.apps.cam.ac.uk/pub/linux/${FAMILY} $DISTRO-security $SECTIONS"
 fi

 cat <<EOF > $DIR/etc/apt/sources.list
deb $BASE
deb-src $BASE

# NB we deliberately include the DIST-updates and DIST-security distributions, otherwise
# 1) we debootstrap the VM
# 2) we run ansible which adds both of these at an early stage
# 3) there are immediately a pile of updates that need applying
deb $UPDATES
deb-src $UPDATES
deb $SECURITY
deb-src $SECURITY

EOF

 # NB deliberately do this right before adding our local repo whic
 # is now https so if we add that before we have apt-transport-https
 # or out-of-date certificates we are not going to get very much further!
 DEBIAN_FRONTEND=noninteractive chroot $DIR /bin/bash -c "apt-get update && apt-get install -y apt-transport-https ca-certificates"

 cat <<EOF >$DIR/etc/apt/sources.list.d/chemistry.list
deb https://downloads.ch.cam.ac.uk/local-debs $DISTRO ucamchem
EOF


chroot $DIR apt-get update
}

# minimum set of packages we need to add in order to then run ansible
function installpackages() {
 local DIR=$1
 local DISTRO=$2
 local FAMILY=$3
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get install -y lsb-release
 # ensure that updates available in our modified sources list entries (after
 # the intial debootstrap) are installed.
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get -y full-upgrade

 # jammy onwards uses zstd-compressed kernels which we need to decompress when using pygrub.
 if [ "$DISTRO" == "jammy" ] || [ "$DISTRO" == "noble" ]; then
   DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get install -y ch-kernel-decompresser
 fi
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get install -y linux-image-generic grub2 initramfs-tools --no-install-recommends

 rmdir $DIR/var/log/journal
}

function installworkstationpackages() {
 local DIR=$1
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get install -y chem-ansible-workstation environment-modules git tcl
 # ensure that updates available in our modified sources list entries (after
 # the intial debootstrap) are installed.
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get -y full-upgrade
}

function writefstab() {
 local DIR=$1
 cat <<EOF >> $DIR/etc/fstab
/dev/xvda1 / ext4 errors=remount-ro 0       1
EOF
}


 ##
 # Bootstrap the basics of a machine into a given directory
 ##
function bootstrap() {
 local BLOCKDEV=$1
 local DISTRO=$2
 local FAMILY=$3
 # Now, prepare the VM and boot it on the network
 # Except we can't PXE boot the damn things.
 echo Bootstrapping a basic OS onto the host
 mkdir -p /mnt/xen
 mount $BLOCKDEV /mnt/xen
if [ "$DISTRO" == "focal" ]; then
  test -e /usr/share/debootstrap/scripts/$DISTRO || SCRIPT=/usr/share/debootstrap/scripts/xenial
  debootstrap --include=udev,openssh-server,python3,python3-apt,python3-httplib2 $DISTRO /mnt/xen http://mirror.apps.cam.ac.uk/pub/linux/ubuntu $SCRIPT
elif [ "$DISTRO" == "jammy" ]; then
  test -e /usr/share/debootstrap/scripts/$DISTRO || SCRIPT=/usr/share/debootstrap/scripts/xenial
  debootstrap --include=udev,openssh-server,python3,python3-apt,python3-httplib2 $DISTRO /mnt/xen http://mirror.apps.cam.ac.uk/pub/linux/ubuntu $SCRIPT
elif [ "$DISTRO" == "noble" ]; then
  test -e /usr/share/debootstrap/scripts/$DISTRO || SCRIPT=/usr/share/debootstrap/scripts/xenial
  debootstrap --include=udev,openssh-server,python3,python3-apt,python3-httplib2 $DISTRO /mnt/xen http://mirror.apps.cam.ac.uk/pub/linux/ubuntu $SCRIPT
elif [ "$FAMILY" == "debian" ]; then
  debootstrap --include=udev,openssh-server,python3,python3-apt,python3-httplib2,netplan.io $DISTRO /mnt/xen http://mirror.apps.cam.ac.uk/pub/linux/debian $SCRIPT
 fi

 IP=`getent hosts $NEWHOST | awk '{print $1}'`

 # Bootstrap network config
 if [[ "$FAMILY" == "debian" ]] || [[ "$DISTRO" == "noble" ]]; then
   INTERFACE=enX0
   ROUTES="use_routes=True"
 else
   INTERFACE=eth0
 fi
 wget -q -O - http://os-installer-utils/osinst/api/getnetplan?hostname=${NEWHOST}\&interface=${INTERFACE}${ROUTES+\&$ROUTES} >/mnt/xen/etc/netplan/01-netcfg.yaml

 # avoid "Netplan configuration should NOT be accessible by others" warning
 chmod 0600 /mnt/xen/etc/netplan/01-netcfg.yaml

 echo $SHORTHOST >/mnt/xen/etc/hostname
 echo $IP $SHORTHOST $NEWHOST >>/mnt/xen/etc/hosts

 # fixup permissions on /tmp
 chmod 1777 /mnt/xen/tmp

}

 ##
 # Put the SSH keys in place
 ##
function sshkeys() {
 local DIR=$1

 IP="$(getent hosts "$NEWHOST" | awk '{print $1}')"
 ssh -o PasswordAuthentication=no root@pxe2025 python3 /usr/local/sbin/fetch_ssh_keys_tgz.py "$IP" || true > "$DIR/keys_package.tar.gz"

 # note that the sshd on the new host won't yet be configured to serve up the signed keys;
 # that is done by ansible

 if [ -s "$DIR/keys_package.tar.gz" ]; then
  (cd "$DIR" && tar -xzf "$DIR/keys_package.tar.gz")
 else
  echo "No ssh keys exist for the host (or otherwise couldn't receive them)"
  echo "These can be setup through ansible"
 fi
 rm -f "$DIR/keys_package.tar.gz"

 # Add ssh authorized keys
 mkdir -p /mnt/xen/root/.ssh
 wget -q -O /mnt/xen/root/.ssh/authorized_keys http://linux-builder.ch.cam.ac.uk/fai/preseed_authorized_keys
 chmod 0600 /mnt/xen/root/.ssh/authorized_keys
}

function getchemgpgkey() {
 local DIR=$1
 wget -O $DIR/etc/apt/trusted.gpg.d/chemistry.gpg https://downloads.ch.cam.ac.uk/hogthrob.gpg
}

function ansibleinitial() {
 local DIR=$1
 cat <<EOF >$DIR/etc/systemd/system/ansible-initial.service
[Unit]
Description=Run ansible on this machine
After=network.target dbus.service
[Service]
ExecStart=/usr/local/sbin/ansible-initial.sh
TTYPath=/dev/tty2
TTYReset=yes
TTYVHangup=yes
User=root
[Install]
WantedBy=multi-user.target default.target
EOF

# chroot $DIR systemctl enable ansible-initial

cat <<EOF >$DIR/usr/local/sbin/ansible-initial.sh
#!/bin/bash
mkdir -p /var/log/chemistry-config

# If ansible has already run, note this
test -f /var/log/chemistry-config/ansible-initial.log && FAILED=true

(
export MODULEPATH=/etc/environment-modules/modules
source /etc/profile.d/modules.sh
module add ansible/workstation
PYTHONUNBUFFERED=1 ansible-pull --full -i hosts -U http://linux-builder.ch.cam.ac.uk/ansible-workstation --extra-vars='chem_safe=true'

# If ansible ran fine
if [ $? -eq 0 ]; then
  /bin/systemctl disable ansible-initial.service
  sleep 10
  reboot
fi
# Reboot if it has only failed once, i.e don't do infinite reboot if something is wrong
if [ -z "$FAILED" ];then
  echo "Going for reboot"
  reboot
fi
echo "Install has failed twice, not rebooting"
) &>> /var/log/chemistry-config/ansible-initial.log
EOF

 chmod +x $DIR/usr/local/sbin/ansible-initial.sh
 # this is what "systemctl enable ansible-initial" does inside the VM, so replicate manually
 mkdir -p $DIR/etc/systemd/system/multi-user.target.wants $DIR/etc/systemd/system/default.target.wants
 ln -s /etc/systemd/system/ansible-initial.service $DIR/etc/systemd/system/multi-user.target.wants/ansible-initial.service
 ln -s /etc/systemd/system/ansible-initial.service $DIR/etc/systemd/system/default.target.wants/ansible-initial.service
}

function setup_grub() {
 local DIR=$1
 # Ensure that update-grub does not add an unwanted boot entry for
 # the dom0's OS. Post-install the grub config files will be templated
 # in ansible
 echo "GRUB_DISABLE_OS_PROBER=true" >> $DIR/etc/default/grub

 for M in proc sys dev ; do
  mount -o bind /$M $DIR/$M
 done

 chroot $DIR update-grub

 for M in proc sys dev ; do
  umount $DIR/$M
 done
 
 if ! $DRBDINUSE ; then
  sed -i 's@root=[^ ][^ ]*@root=/dev/xvda1@' ${DIR}/boot/grub/grub.cfg
 fi
}

### Main code

# Check running as root

if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit 1
fi


# Set defaults
MEMORY=768
DISKSIZE=10G
VGROUP=`vgdisplay | grep VG.Name |awk ' { print $3 ; } '| tail -1`
OVERWRITE=false
DRBD_PRIORITY=10
DIST=noble
FAMILY=ubuntu
WORKSTATION=false
REINSTALL=no
FORCE=''
CPUS=1
LVNAME=

# Process options
while getopts "h:s:p:v:om:d:c:rwkl:x" Option ; do
 case $Option in
  h) NEWHOST=$OPTARG ;;
  s) DISKSIZE=${OPTARG}G ;;
  p) DRBD_PRIORITY=$OPTARG ;;
  v) VGROUP=$OPTARG ;;
  o) OVERWRITE=true ;;
  m) MEMORY=$OPTARG ;;
  d) DIST=$OPTARG
  if ! [[ "$DIST" == "noble" || "$DIST" == "jammy" || "$DIST" == "focal" || "$DIST" == "bookworm" || "$DIST" == "none" ]]; then
    echo Unknown dist $OPTARG ; exit 3
  fi
  ;;
  r) REINSTALL=yes ; FORCE='-F' ;;
  w) WORKSTATION=true ;;
  c) CPUS=$OPTARG ;;
  l) LVNAME=$OPTARG ;;
  x) set -x ;;
  *) echo Unknown option ; usage; exit 3;;
 esac
done

shift $(($OPTIND - 1));

if [ ! -z "$*" ]; then
  echo "Unhandled arguments: $*"
  usage
  exit 3
fi

if [[ "$DIST" == "bookworm" ]] ; then
  FAMILY=debian
fi

# Here are some basic checks
if [ -z $NEWHOST ] ; then
 echo No hostname provided
 usage
 exit 1
fi

# Get MAC address from database
if [ -z $MACADDR ] ; then
   ensuremac $NEWHOST
fi
checkuniquemac $MACADDR


# Not quite sure where the threshold is but with 512M, ansible-initial fails at the
# point of installed all the latex packages (and presumably then running one of their
# postinst scripts).
if [ $MEMORY -lt 1024 ] && [ $WORKSTATION == true ] ; then
  echo $MEMORY MB of memory is not enough for a workstation\; try specifying at least 1024
  exit 1
fi

# Workstations need a lot more disk than servers
if [ ${DISKSIZE%G} -lt 50 ] && [ $WORKSTATION == true ] ; then
  echo $DISKSIZE of disk is not enough for a workstation\; try specifying at least 50
  exit 1
fi

# e.g. when using pygrub, we need at least a couple of GB to install kernels and ancilliary
# packages. Note that we have long assumed elsewhere in the script that DISKSIZE is a string
# of the form "10G" so we strip the last char to do this size check.
if [ ${DISKSIZE::-1} -lt 5 ]; then
  echo DISKSIZE too small \(must be at least 5 GB\)
  exit 1
fi

ensuredns $NEWHOST
ensurenovm $NEWHOST
SHORTHOST=${NEWHOST/.*/}
warnifnametoolong $SHORTHOST
if [ -z "$LVNAME" ]; then
  LVNAME="$SHORTHOST"
fi
checkdrbd

getvlan


# Debug: display all the settings:
echo Hostname $NEWHOST, [DISKSIZE=$DISKSIZE] MACADDR=$MACADDR [MEMORY=$MEMORY] [VGROUP=$VGROUP] LVNAME=$LVNAME VLAN=$VLAN REINSTALL=$REINSTALL CPUS=$CPUS
export SHORTHOST NEWHOST LVNAME DISKSIZE MACADDR MEMORY OVERWRITE REINSTALL CPUS

# Check we don't already have a DRBD device of that name unless just reinstalling
if [[ "$REINSTALL" == "no" ]] ; then
 if $DRBDINUSE ; then
  ensurenodrbd $LVNAME
 fi
fi

if [[ "$REINSTALL" == "no" ]] ; then
 # Create the logical volume(s) unless just reinstalling
 createlv $LVNAME $VGROUP $DISKSIZE
 if $DRBDINUSE ; then
  # Create the DRBD device also
  createdrbd $LVNAME
 fi
fi

getblockdev

# Work on the config for the newly-built DomU
writexenconfig $DIST
if $DRBDINUSE ; then 
  syncxenconf 
fi

if [[ "$DIST" == "none" ]]; then
  echo "You'll need to edit /etc/xen/$NEWHOST"
  echo "to refer to the installation media."
  echo "And start the VM yourself."
  if $DRBDINUSE ; then
    if [[ "$REINSTALL" == "no" ]] ; then
      echo "Once setup, please edit /etc/ha.d/haresources manually."
    fi
  else
    echo "Once setup, please create the symlink in /etc/xen/auto/ manually."
  fi
  exit
fi

# Create root filesystem
mke2fs $FORCE -t ext4 $BLOCKDEV

# Work on the newly-built DomU
bootstrap $BLOCKDEV $DIST $FAMILY
sshkeys /mnt/xen/etc/ssh
writefstab /mnt/xen
gettyonconsole /mnt/xen
getchemgpgkey /mnt/xen
sourceslist /mnt/xen $DIST $FAMILY
installpackages /mnt/xen $DIST $FAMILY
if ($WORKSTATION) ; then
 installworkstationpackages /mnt/xen
 ansibleinitial /mnt/xen
fi
setup_grub /mnt/xen
unmountchroot /mnt/xen

# Finalise configuration
if $DRBDINUSE ; then
 if [[ "$REINSTALL" == "no" ]] ; then
  configha
 fi
 sleep 3
 drbdadm secondary $LVNAME
else
 mkdir -p /etc/xen/auto
 ln -sf ../$NEWHOST /etc/xen/auto/$NEWHOST
fi

# Start the VM 
$XM create /etc/xen/$NEWHOST
